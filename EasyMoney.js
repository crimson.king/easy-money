/*
 * Copyright 2021 Douglas Silva
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @param {HTMLInputElement|string} selectorOrElement - The input element itself or a CSS selector for it
 * @param {Object} options - Options for this instance
 * @param {string} options.locale - Locale (to configure Number.prototype.toLocaleString)
 * @param {string} options.currency - Currency (to configure Number.prototype.toLocaleString)
 */
function EasyMoney(selectorOrElement, options) {
  this.input = selectorOrElement;

  if (typeof selectorOrElement === 'string') {
    this.input = document.querySelector(selectorOrElement);
  }
  
  if (!this.input) {
    throw new Error('Target element not found');
  }
  
  if (false === this.input instanceof HTMLInputElement) {
    throw new Error('Target element is not an input element');
  }
  
  // Attempt to find the label
  if (this.input.hasAttribute('id')) {
    this.label = document.querySelector(`label[for="${this.input.getAttribute('id')}"]`);
  } else {
    this.label = this.input.parentElement;
  }
  
  if (false === this.label instanceof HTMLLabelElement) {
    throw new Error('Failed to locate a label for the target element');
  }
  
  // Default configuration
  this.options = Object.assign({
    locale: 'en-US',
    currency: 'USD'
  }, options);
  
  // Initial update
  this.updateLabel();
  
  this.setupEvents();
}

EasyMoney.prototype.setupEvents = function() {
  this.input.addEventListener('input', function(event) {
    this.updateLabel();
  }.bind(this));
}

/**
 * Read the current value and display it properly formatted
 * on the label.
 */
EasyMoney.prototype.updateLabel = function() {
  this.prepareContainer();
  
  if (this.input.value === '') {
    return false;
  }
  
  let value = parseFloat(this.input.value);
  
  if (isNaN(value)) {
    return false;
  }
  
  value = value.toLocaleString(this.options.locale, {style: 'currency', currency: this.options.currency});
  this.label.querySelector('.easy-money-container').textContent = ` (${value})`;
}

/**
 * Reset the label, creating the container element if it
 * doesn't yet exist.
 */
EasyMoney.prototype.prepareContainer = function() {
  var container = this.label.querySelector('.easy-money-container');

  if (!container) {
    container = document.createElement('span');
    container.classList.add('easy-money-container');
    container.setAttribute('aria-hidden', 'true');
    
    this.label.appendChild(container);
  }
  
  container.textContent = '';
}

export { EasyMoney };

