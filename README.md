# EasyMoney
A helper script for money input.

## Features
- Doesn't break accessibility.
- Builds on top of native HTML features.

## How it works
The value of the input is read, formatted and displayed on the label. The input itself is never changed. The purpose of this script is merely to facilitate visualization of monetary values.

## Installation

```
npm install @crimson_king/easy-money
```

## Using it

1. Use `<input type="number">` and adjust the `step` attribute to get a decimal.

```html
<label for="example">Example</label>
<input id="example" type="number" step="0.01">
```

2. Import the module and create an instance, specifying the desired locale and currency (as in `Number.prototype.toLocaleString`).
```js
import { EasyMoney } from './EasyMoney.js';

let easyMoney = new EasyMoney('#example', {
  locale: 'pt-BR',
  currency: 'BRL'
});
```

## License
Copyright 2021 Douglas Silva

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

